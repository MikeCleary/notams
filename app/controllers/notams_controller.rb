class NotamsController < ApplicationController
  def new
  end

  def create
  	Notam.parse( params[:notams_data] )
  	redirect_to notams_path
  end

  def index
  	@notams = Notam.all 
  end
end
