class Notam < ActiveRecord::Base

	attr_accessor :data_string

	DAYS = [ :mon, :tue, :wed, :thu, :fri, :sat, :sun ]

	def self.parse( data )
		notam_array = split_and_validate( data )
		notam_array.each do |notam|
			create_from_string( notam )
		end
	end

	def self.create_from_string( data_string )
		words = data_string.split( ' ' )
		notam = Notam.new( icao: words[ words.index( 'A)' ) + 1 ], data_string: data_string )
		notam.get_aerodrome_hours_of_ops
		notam.save
	end

	def get_aerodrome_hours_of_ops
		matches = day_match_data
		# makes a hash of empty days
		days_with_times = Hash[ DAYS.map {|day| [day, nil]} ]
		matches.each_with_index do | match_data, index |
			# gets the index of the end of this match
			start_index = match_data.end( 0 )
			# gets the index of the start of the next match, or the end of the string
			end_index = matches[ index + 1 ] ? matches[ index + 1 ].begin(0) : -1
			# slices notams_description with these indexes 
			days_with_times[match_data.to_s.to_sym.downcase] = notam_description[start_index ... end_index ]
			.strip
			.gsub(/[^\P{P}-]+/ , "") 
		end
		self.assign_attributes( fill_blanks(days_with_times) )
	end

	# If attributes hash dash replace with the next correctly filled entry
	# 
	def fill_blanks( days_with_times )
		DAYS.each do |day|
			if days_with_times[day] == "-"
				days_with_times[day] = get_next( day, days_with_times )
			end
		end
		days_with_times
	end

	def get_next( day, days_with_times )
		next_day = DAYS[ DAYS.index(day) +1 ]
		next_day_time = days_with_times[ next_day ] 
		if next_day_time
			next_day_time
		else
			days_with_times[next_day] = get_next( next_day, days_with_times )
		end
	end

	# returns an array of match data objects from the notam description
	# 
	def day_match_data
		matches = []
		DAYS.each do |day|
			if match = notam_description.match( day.to_s.upcase )
				matches << match
			end
		end
		matches
	end

	def notam_description
		data_string.split("OPS/SERVICE")[1].split("CREATED")[0]
	end

	private

	def self.split_and_validate( data )
		notam_array = data.split(/\r\n\r\n/)
		valid_notam_array =	notam_array.select{ |notam| notam.include?"AERODROME HOURS OF OPS/SERVICE" }
	end

end
