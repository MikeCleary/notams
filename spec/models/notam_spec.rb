require 'rails_helper'

RSpec.describe Notam, type: :model do
	describe 'self#parse' do 
		before do 
			notam_data = File.read( 'spec/fixtures/notam_data.txt' ).gsub( /\n/, "\r\n" )
			Notam.parse( notam_data )
		end

		it "should make some notams" do 
			expect( Notam.count ).to eq 10
		end

		it "should get an ICAO" do 
			expect( Notam.first.icao ).to eq( "ESGJ" )
			expect( Notam.last.icao ).to eq( "ESSV" )
		end

		it "should get simple days of the week" do 
			expect( Notam.first.fri ).to eq( "0730-2100" )
			expect( Notam.first.sun ).to eq( "CLOSED" )
			
			expect( Notam.last.fri ).to eq( "0530-2000" )
			expect( Notam.last.sat ).to eq( "0730-1830" )
		end

		it "should fill in ranged times" do 
			expect( Notam.last.mon ).to eq( "0530-2115" )
			expect( Notam.last.tue ).to eq( "0530-2115" )
			expect( Notam.last.wed ).to eq( "0530-2115" )
		end 
	end
end
