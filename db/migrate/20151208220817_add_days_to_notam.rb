class AddDaysToNotam < ActiveRecord::Migration
  def change
    add_column :notams, :mon, :string
    add_column :notams, :tue, :string
    add_column :notams, :wed, :string
    add_column :notams, :thu, :string
    add_column :notams, :fri, :string
    add_column :notams, :sat, :string
    add_column :notams, :sun, :string
  end
end
