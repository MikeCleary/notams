class AddIcaoToNotams < ActiveRecord::Migration
  def change
    add_column :notams, :icao, :string
  end
end
